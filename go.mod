module gitlab.com/gitlab-org/gitlab-elasticsearch-indexer

require (
	github.com/aws/aws-sdk-go v1.8.10
	github.com/deoxxa/aws_signing_client v0.0.0-20161109131055-c20ee106809e
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/stretchr/testify v1.2.2
	gitlab.com/gitlab-org/gitaly v0.126.0
	gitlab.com/gitlab-org/gitaly-proto/go/gitalypb v0.0.0-20190517173708-b46bc4dfbcb0
	gitlab.com/lupine/icu v1.0.0
	golang.org/x/net v0.0.0-20190514140710-3ec191127204
	google.golang.org/grpc v1.20.1
	gopkg.in/ini.v1 v1.42.0 // indirect
	gopkg.in/olivere/elastic.v5 v5.0.80
)
